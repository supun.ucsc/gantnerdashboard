FROM node:8.6.0-alpine

WORKDIR /app

COPY package.json .
COPY package-lock.json .
RUN npm install
RUN npm install react-scripts --save

COPY . /app

EXPOSE 3000



CMD  npm start
