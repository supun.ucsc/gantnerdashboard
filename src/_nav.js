export default {
  items: [
    {
      name: 'Gauges',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'info',
       },
    },



    {
      name: 'Charts',
      url: '/',
      icon: 'icon-chart',
      children: [
        {
          name: 'Cooling Tower',
          url: '/coolingtower',
          icon: '',
        },
        {
          name: 'FFT',
          url: '/fft',
          icon: '',
        },
        {
          name: 'default Chart',
          url: '/default',
          icon: '',
        },
        {
          name: 'Trend Parameter(Avg)',
          url: '/trendavg',
          icon: '',
        },
        {
          name: 'Trend Parameter',
          url: '/trendparam',
          icon: '',
        },
        {
          name: 'Configuration',
          url: '/configuration',
          icon: '',
        },

      ],
    },
    {
      name: 'Alerts',
      url: '/alerts',
      icon: 'icon-bell',

    },
    {
      name: 'Reports',
      url: '/reports',
      icon: 'icon-layers',

    },

    {
      name: 'Channels',
      url: '/channels',
      icon: 'icon-list',

    },
    {
      name: 'Export',
      url: '/export',
      icon: 'icon-cloud-download',

    },
    {
      name: 'Installation info',
      url: '/installationinfo',
      icon: 'icon-info',

    },



  ],
};
