import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

import usersData from './UsersData'
import { Bar, Doughnut, Line, Pie, Polar, Radar } from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';

import C3Chart from "react-c3js";

function UserRow(props) {
  const user = props.user
  const userLink = `/users/${user.id}`

  const getBadge = (status) => {
    return status === 'Active' ? 'success' :
      status === 'Inactive' ? 'secondary' :
        status === 'Pending' ? 'warning' :
          status === 'Banned' ? 'danger' :
            'primary'
  }



  return (
    <tr key={user.id.toString()}>
      <th scope="row"><Link to={userLink}>{user.id}</Link></th>
      <td><Link to={userLink}>{user.name}</Link></td>
      <td>{user.registered}</td>
      <td>{user.role}</td>
      <td><Link to={userLink}><Badge color={getBadge(user.status)}>{user.status}</Badge></Link></td>
    </tr>
  )
}



const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  scales: {
          xAxes: [{ barThickness: 2,scaleLabel: {
					display: true,
					labelString: 'Frequency'
				}, }],
          yAxes: [
        {
          gridLines: {
                lineWidth: 0
            },
            scaleLabel: {
  					display: true,
  					labelString: 'Amplitude'
  				}
        }
      ]
      },
  maintainAspectRatio: false
}

class Users extends Component {


  state = {
    barData:    {
        labels: ['50', '100', '150', '200', '250', '300', '350','400','450','500','550','600'],
        datasets: [
          {
            label: 'FFT',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: [65, 59, 80, 81, 56, 55, 40],
          },
        ],
      },
    fif: 0.001,
    hun: 0.0005,
    onefif: 0.0006,
    twoo: 0.0008,
    twofif: 0.0003,
    three: 0.000523,
    threefif: 0.00053,
    fourh: 0.000545,
    fourfif: 0.000532,
    fivehun: 0.000545,
    fivefif: 0.000554,
    sixh: 0.00054,
  }

  start() {
    const obj=this;


        setTimeout(function() {
          console.log('Hello My Infinite Loop Execution');

        // Again

       obj.generateRandomNumber()
        obj.start();

        // Every 3 sec
      }, 1000);
  }

  componentDidMount(){

    const obj=this;
    obj.start();


   }

  generateRandomNumber() {

        const obj=this;

         var temp=obj.state.barData;


         var dataArray=[]

         for(var i=0;i<12;i++){

           dataArray.push( Math.random() * (0.00125 - 0) + 0)
         }

          temp.datasets[0].data=dataArray;

         obj.setState({barData:temp})

      //alert(highlightedNumber);
  };
  render() {

    const userList = usersData.filter((user) => user.id < 10)

    return (
      <div className="animated fadeIn">
      <h2 style={{paddingBottom:10}}>FFT CoolingTower</h2>

        <Row>

        <Col xs="6" lg="3">
          <Card>
            <CardHeader>

            </CardHeader>
            <CardBody>
              <Table responsive bordered>
                <thead>
                <tr>
                  <th>Serial Number</th>
                  <th>751267</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Location</td>
                  <td>Glcms</td>

                </tr>
                <tr>
                  <td>Time</td>
                  <td>20180502_092200</td>

                </tr>
                <tr>
                  <td>Channel</td>
                  <td>FFT_Spectrum_10</td>

                </tr>
                <tr>
                  <td>Number of bins</td>
                  <td>1639</td>

                </tr>

                </tbody>
              </Table>

            </CardBody>

          </Card>
        </Col>

                  </Row>
                  <div  style={{backgroundColor:'white',height:300}}className="chart-wrapper">
                    <Bar data={this.state.barData} options={options} />
                  </div>
      </div>
    )
  }
}

export default Users;
