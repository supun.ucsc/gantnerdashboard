import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

import usersData from './UsersData'
import C3Chart from "react-c3js";

function UserRow(props) {
  const user = props.user
  const userLink = `/users/${user.id}`

  const getBadge = (status) => {
    return status === 'Active' ? 'success' :
      status === 'Inactive' ? 'secondary' :
        status === 'Pending' ? 'warning' :
          status === 'Banned' ? 'danger' :
            'primary'
  }

  return (
    <tr key={user.id.toString()}>
      <th scope="row"><Link to={userLink}>{user.id}</Link></th>
      <td><Link to={userLink}>{user.name}</Link></td>
      <td>{user.registered}</td>
      <td>{user.role}</td>
      <td><Link to={userLink}><Badge color={getBadge(user.status)}>{user.status}</Badge></Link></td>
    </tr>
  )
}

class Users extends Component {

  state = {
    crest: 6.482,
    peak: 0.011,
    rms: 0.02,
    velocity: 0.02,
  }

  start() {
    const obj=this;
        setTimeout(function() {
          console.log('Hello My Infinite Loop Execution');

        // Again

        obj.generateRandomNumber()
        obj.start();

        // Every 3 sec
      }, 1000);
  }

  componentDidMount(){

    const obj=this;
    obj.start();


   }

  generateRandomNumber() {

        const obj=this;
        var  highlightedNumber = Math.random() * (600 - 0) + 0;
        obj.setState({crest:highlightedNumber.toFixed(2)})

        var  highlightedNumber = Math.random() * (2.4 - 0) + 0;
        obj.setState({peak:highlightedNumber.toFixed(2)})

        var  highlightedNumber = Math.random() * (1.2 - 0) + 0;
        obj.setState({rms:highlightedNumber.toFixed(2)})

        var  highlightedNumber = Math.random() * (1.2 - 0) + 0;
        obj.setState({velocity:highlightedNumber.toFixed(2)})

      //alert(highlightedNumber);
  };
  render() {

    const userList = usersData.filter((user) => user.id < 10)

    return (
      <div className="animated fadeIn">
        <h2 style={{paddingBottom:10}}>Status CoolingTower</h2>
        <Row>
          <Col xl={3}>
            <Card>
              <CardHeader>
                 CREST <small className="text-muted">7_CoolTow_Crest_10</small>

                 <C3Chart
                   style={{ height: "12rem" ,marginLeft:-12}}
                   data={



                     {
                     columns: [
                       // each columns data
                       ["data1", this.state.crest],
                      ],
                     type: 'gauge',


                     colors: {
                       data1: "orange",
                       data2: "orange",
                      },
                     names: {
                       // name of each serie
                       data1: "PEAK",
                      },


                   }

                 }
                 gauge= {{
                     label: {
                         format: function(value, ratio) {
                             return value+"";
                         },
                         show: true // to turn off the min/max labels.
                     },
                 min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                 max: 600, // 100 is default
                 units: '[g]',
                 width: 39 // for adjusting arc thickness
               }}
                 labels={ {
                   format: function (value) { return 20 }

                 }}
                   legend={{
                     show: false, //hide legend
                   }}
                   padding={{
                     bottom: 0,
                     top: 0,
                   }}
                 />

              </CardHeader>
              <CardBody>

              </CardBody>
            </Card>
          </Col>
          <Col xl={3}>
            <Card>
              <CardHeader>
                 PEAK <small className="text-muted">7_CoolTow_Peak_10</small>

                 <C3Chart
                   style={{ height: "12rem" }}
                   data={



                     {
                     columns: [
                       // each columns data
                       ["data1", this.state.peak],
                      ],
                     type: 'gauge',


                     colors: {
                       data1: "orange",
                       data2: "orange",
                      },
                     names: {
                       // name of each serie
                       data1: "PEAK",
                      },


                   }

                 }
                 gauge= {{
                     label: {
                         format: function(value, ratio) {
                             return value+"";
                         },
                         show: true // to turn off the min/max labels.
                     },
                 min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                 max: 2.4, // 100 is default
                 units: '[g]',
                 width: 39 // for adjusting arc thickness
               }}
                 labels={ {
                   format: function (value) { return 20 }

                 }}
                   legend={{
                     show: false, //hide legend
                   }}
                   padding={{
                     bottom: 0,
                     top: 0,
                   }}
                 />

              </CardHeader>
              <CardBody>

              </CardBody>
            </Card>
          </Col>
          <Col xl={3}>
            <Card>
              <CardHeader>
                 RMS <small className="text-muted">7_CoolTow_RMS_10</small>

                 <C3Chart
                   style={{ height: "12rem" }}
                   data={



                     {
                     columns: [
                       // each columns data
                       ["data1", this.state.rms],
                      ],
                     type: 'gauge',


                     colors: {
                       data1: "orange",
                       data2: "orange",
                      },
                     names: {
                       // name of each serie
                       data1: "PEAK",
                      },


                   }

                 }
                 gauge= {{
                     label: {
                         format: function(value, ratio) {
                             return value+"";
                         },
                         show: true // to turn off the min/max labels.
                     },
                 min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                 max: 1.2, // 100 is default
                 units: '[g]',
                 width: 39 // for adjusting arc thickness
               }}
                 labels={ {
                   format: function (value) { return 20 }

                 }}
                   legend={{
                     show: false, //hide legend
                   }}
                   padding={{
                     bottom: 0,
                     top: 0,
                   }}
                 />

              </CardHeader>
              <CardBody>

              </CardBody>
            </Card>
          </Col>
          <Col xl={3}>
            <Card>
              <CardHeader>
                 VELOCITY <small className="text-muted">7_CoolTow_Velocity_10</small>

                 <C3Chart
                   style={{ height: "12rem" }}
                   data={



                     {
                     columns: [
                       // each columns data
                       ["data1", this.state.velocity],
                      ],
                     type: 'gauge',


                     colors: {
                       data1: "orange",
                       data2: "orange",
                      },
                     names: {
                       // name of each serie
                       data1: "PEAK",
                      },


                   }

                 }
                 gauge= {{
                     label: {
                         format: function(value, ratio) {
                             return value+"";
                         },
                         show: true // to turn off the min/max labels.
                     },
                 min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
                 max: 1.2, // 100 is default
                 units: '[g]',
                 width: 39 // for adjusting arc thickness
               }}
                 labels={ {
                   format: function (value) { return 20 }

                 }}
                   legend={{
                     show: false, //hide legend
                   }}
                   padding={{
                     bottom: 0,
                     top: 0,
                   }}
                 />

              </CardHeader>
              <CardBody>

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Users;
